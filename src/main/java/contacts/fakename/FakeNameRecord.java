package contacts.fakename;

import java.io.Serializable;

/**
 */
public class FakeNameRecord implements Serializable {

    public static final String P_NUMBER = "number";
    public static final String P_GENDER = "gender";
    public static final String P_GIVEN_NAME = "givenName";
    public static final String P_MIDDLE_INITIAL = "middleInitial";
    public static final String P_SURNAME = "surname";
    public static final String P_STREET_ADDRESS = "streetAddress";
    public static final String P_CITY = "city";
    public static final String P_STATE = "state";
    public static final String P_ZIP_CODE = "zipCode";
    public static final String P_COUNTRY = "country";
    public static final String P_EMAIL_ADDRESS = "emailAddress";
    public static final String P_PASSWORD = "password";
    public static final String P_TELEPHONE_NUMBER = "telephoneNumber";
    public static final String P_MOTHERS_MAIDEN = "mothersMaiden";
    public static final String P_BIRTHDAY = "birthday";
    public static final String P_CC_TYPE = "ccType";
    public static final String P_CC_NUMBER = "ccNumber";
    public static final String P_CVV2 = "cvv2";
    public static final String P_CC_EXPIRES = "ccExpires";
    public static final String P_NATIONAL_ID = "nationalID";
    public static final String P_OCCUPATION = "occupation";

    private int number;
    private String gender;
    private String givenName;
    private String middleInitial;
    private String surname;
    private String streetAddress;
    private String city;
    private String state;
    private String zipCode;
    private String country;
    private String emailAddress;
    private String password;
    private String telephoneNumber;
    private String mothersMaiden;
    private String birthday;
    private String ccType;
    private String ccNumber;
    private String cvv2;
    private String ccExpires;
    private String nationalID;
    private String occupation;

    public static final String[] ALL_PROPERTIES = {
        P_NUMBER,
        P_GENDER,
        P_GIVEN_NAME,
        P_MIDDLE_INITIAL,
        P_SURNAME,
        P_STREET_ADDRESS,
        P_CITY,
        P_STATE,
        P_ZIP_CODE,
        P_COUNTRY,
        P_EMAIL_ADDRESS,
        P_PASSWORD,
        P_TELEPHONE_NUMBER,
        P_MOTHERS_MAIDEN,
        P_BIRTHDAY,
        P_CC_TYPE,
        P_CC_NUMBER,
        P_CVV2,
        P_CC_EXPIRES,
        P_NATIONAL_ID,
        P_OCCUPATION,
    };


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getMothersMaiden() {
        return mothersMaiden;
    }

    public void setMothersMaiden(String mothersMaiden) {
        this.mothersMaiden = mothersMaiden;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getCcExpires() {
        return ccExpires;
    }

    public void setCcExpires(String ccExpires) {
        this.ccExpires = ccExpires;
    }

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}

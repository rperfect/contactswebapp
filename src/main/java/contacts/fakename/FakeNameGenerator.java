package contacts.fakename;


import contacts.util.ReflectionUtils;

import java.beans.PropertyDescriptor;
import java.io.File;

/**
 */
public class FakeNameGenerator {

    private File dataFile;


    public FakeNameGenerator() {
        this(new File("FakeNameData.csv"));
    }

    private FakeNameGenerator(File dataFile) {
        this.dataFile = dataFile;
    }

    public FakeNameRecord generateOneRecord() {
        return generateRecords(1)[0];
    }

    public FakeNameRecord[] generateRecords(int requiredCount) {

        PersistentLineReader lineReader = new PersistentLineReader(dataFile);
        String[] lines = lineReader.readLines(requiredCount);
        FakeNameRecord[] records = convertLines(lines);

        return records;
    }

    private FakeNameRecord[] convertLines(String[] lines) {
        FakeNameRecord[] records = new FakeNameRecord[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            String[] tokens = line.split(",");

            FakeNameRecord record = new FakeNameRecord();
            for(int j = 0; j < FakeNameRecord.ALL_PROPERTIES.length; j++) {
                String propertyName = FakeNameRecord.ALL_PROPERTIES[j];
                String tokenValue = tokens[j];

                PropertyDescriptor descriptor = ReflectionUtils.getPropertyDescriptor(FakeNameRecord.class, propertyName);
                Class propertyType = descriptor.getPropertyType();
                Object convertedValue = convertToken(propertyType, tokenValue);
                ReflectionUtils.invoke(descriptor.getWriteMethod(), record, new Object[]{convertedValue});
            }

            records[i] = record;
        }
        return records;
    }


    private Object convertToken(Class propertyType, String tokenValue) {

        Object result = null;
        if(tokenValue != null && tokenValue.trim().length() > 0) {
            if(propertyType.equals(String.class)) {
                result = tokenValue;
            }
            else if(propertyType.equals(Integer.TYPE)) {
                result = Integer.parseInt(tokenValue);
            }
            else {
                throw new RuntimeException("Unknown property type of " + propertyType.getName());
            }
        }

        return result;
    }


    public static void main(String[] args) {
        File dataFile = new File("FakeNameData.csv");
        FakeNameGenerator generator = new FakeNameGenerator(dataFile);
        FakeNameRecord[] fakeNameRecords = generator.generateRecords(5);
        for(FakeNameRecord nextRecord : fakeNameRecords) {
            System.out.println(nextRecord.getGivenName() + " " + nextRecord.getSurname() + ", " + nextRecord.getBirthday());
        }
    }
}

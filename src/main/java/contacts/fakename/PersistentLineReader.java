package contacts.fakename;

import java.io.*;
import java.util.Properties;

/**
 * This class reads lines from a file and saves the position of where it has read to be used as the
 * starting point for the next time it is called.
 */
public class PersistentLineReader {

    private File propertiesFile = new File(PersistentLineReader.class.getSimpleName() + ".properties");
    private File dataFile;
    private long position;


    public PersistentLineReader(File dataFile) {
        this.dataFile = dataFile;
    }

    private void loadPosition() {
        try {
            if(propertiesFile.exists()) {
                Properties properties = new Properties();
                InputStream in = new FileInputStream(propertiesFile);
                properties.load(in);
                position = Long.parseLong(properties.getProperty("position", "0"));
                in.close();
            }
        }
        catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void savePosition() {
        try {
            Properties properties = new Properties();
            properties.setProperty("position", String.valueOf(position));
            OutputStream out = new FileOutputStream(propertiesFile);
            properties.store(out, "PersistentLineReader properties");
            out.close();
        }
        catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String[] readLines(int requiredCount) {
        return readLinesFromResource(requiredCount);
    }


    private String[] readLinesFromResource(int requiredCount) {
        try {
            String[] lines = new String[requiredCount];
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(getClass().getResourceAsStream("/FakeNameData.csv")));
            // discard the first line as it contains header
            reader.readLine();
            for(int i = 0; i < requiredCount; i++) {
                String nextLine = reader.readLine();
                lines[i] = nextLine;
            }
            return lines;
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String[] readLinesFromRandomAccessFile(int requiredCount) {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(dataFile, "r");

            // reposition to where we last finished reading
            loadPosition();

            if(position == 0) {
                randomAccessFile.readLine();
            }
            else {
                randomAccessFile.seek(position);
            }

            String[] lines = new String[requiredCount];

            for(int i = 0; i < requiredCount; i++) {
                String nextLine = randomAccessFile.readLine();
                if(nextLine != null && nextLine.length() > 0) {
                    lines[i] = nextLine;
                }
                else {
                    randomAccessFile.seek(0);
                    randomAccessFile.readLine(); // skip first line
                    i--;
                }
            }


            position = randomAccessFile.getFilePointer();

            randomAccessFile.close();
            savePosition();
            
            return lines;
        }
        catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {

        File dataFile = new File("5e2da64d.csv");
        PersistentLineReader lineReader = new PersistentLineReader(dataFile);
        String[] lines = lineReader.readLines(9);

        for(String nextLine : lines) {
            System.out.println(nextLine);
        }
    }

}

package contacts.domain;

/**
 */
public class ContactSearchCriteria {

    private String name;
    private PageState pageState;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PageState getPageState() {
        return pageState;
    }

    public void setPageState(PageState pageState) {
        this.pageState = pageState;
    }
}

package contacts.domain;

import java.util.List;

/**
 */
public class ContactSearchResult {

    private PageState pageState;
    private List<ContactModel> contacts;


    public PageState getPageState() {
        return pageState;
    }

    public void setPageState(PageState pageState) {
        this.pageState = pageState;
    }

    public List<ContactModel> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactModel> contacts) {
        this.contacts = contacts;
    }
}

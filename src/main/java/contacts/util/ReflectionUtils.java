package contacts.util;


import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 */
public class ReflectionUtils implements Serializable {

    public static Class forName(String classname) {
        try {
            return Class.forName(classname);
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object newInstance(String className) {
        try {
            Class objectClass = Class.forName(className);
            return newInstance(objectClass);
        }
        catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> T newInstance(Class<T> objectClass) {
        try {
            return objectClass.newInstance();
        }
        catch(IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
        catch(InstantiationException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Object invoke(Method method, Object object, Object[] args) {
        try {
            Object result = method.invoke(object, args);
            return result;
        }
        catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
        catch (IllegalArgumentException ex) {
            throw new RuntimeException(ex);
        }
        catch (InvocationTargetException ex) {
            if(ex.getTargetException() != null && ex.getTargetException() instanceof RuntimeException) {
                RuntimeException targetException = (RuntimeException)ex.getTargetException();
                throw targetException;
            }
            else {
                throw new RuntimeException(ex);
            }
        }
    }

    public static Object invokeMethod(String methodName, Object object) {
        Method method = getMethod(object.getClass(), methodName, (Class[])null);
        return invoke(method, object, (Object[])null);
    }

    public static Method getMethod(Class objectClass, String methodName, Class[] parameters) {
        try {
            return objectClass.getMethod(methodName, parameters);
        }
        catch (NoSuchMethodException ex) {
            throw new RuntimeException(ex);
        }
        catch (SecurityException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static BeanInfo getBeanInfo(Class objectClass) {
        try {
            return Introspector.getBeanInfo(objectClass);
        }
        catch (IntrospectionException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static PropertyDescriptor getPropertyDescriptor(Class objectClass, String propertyName) {
        BeanInfo beanInfo = getBeanInfo(objectClass);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        PropertyDescriptor descriptor = null;
        for(PropertyDescriptor nextDescriptor : propertyDescriptors) {
            if(nextDescriptor.getName().equals(propertyName)) {
                descriptor = nextDescriptor;
                break;
            }
        }
        return descriptor;
    }


}

'use strict';

/* Controllers */

function ContactSearchController($scope, $http, contactSearchService) {


    $scope.contacts = contactSearchService().contacts;
    $scope.pageState = contactSearchService().pageState;
    $scope.pageNumbers = [];


    $scope.previousPage = function() {
        $scope.setPageNumber($scope.pageState.pageNumber - 1);
    };

    $scope.nextPage = function() {
        $scope.setPageNumber($scope.pageState.pageNumber + 1);
    };

    $scope.setPageNumber = function(nextPageNumber) {

        // the service will fix any range issues with pageNumber
        $scope.pageState.pageNumber = nextPageNumber;

        // do the search
        $scope.search();
    };

    $scope.search = function() {

        $http.get('rest/backend/contact?name=' + $scope.name + '&pageNumber=' + $scope.pageState.pageNumber + '&pageSize=' + $scope.pageState.pageSize).success(function(data) {
            contactSearchService().contacts = data.contacts;
            contactSearchService().pageState = data.pageState;

            $scope.contacts = data.contacts;
            $scope.pageState = data.pageState;

            $scope.pageNumbers = [];
            for(var nextPageNum = 1; nextPageNum <= data.pageState.totalPageCount; nextPageNum++) {
                $scope.pageNumbers.push({
                    pageNum : nextPageNum,
                    pageClass : ''
                });
            }

            for(var i = 0; i < $scope.pageNumbers.length; i++) {
                var pageMarker = $scope.pageNumbers[i];
                if(i === $scope.pageState.pageNumber) {
                    pageMarker.pageClass = 'active';
                }
                else {
                    pageMarker.pageClass = '';
                }
            }

            console.log('pageNumber = ' + $scope.pageState.pageNumber);
        });
    };

    $scope.search();
}
ContactSearchController.$inject = ['$scope', '$http', 'contactSearchService'];

/**
 * Edit functions. Just some more comments.
 *
 * @param $scope
 * @param $routeParams
 * @param $resource
 * @constructor
 */
function ContactEditController($scope, $routeParams, $resource) {

    $scope.contactId = $routeParams.contactId;

    this.contactsResource = $resource('rest/backend/contact/:cid', {cid: '@cid'});
    $scope.selectedContact = this.contactsResource.get({cid: $routeParams.contactId});
    console.log($scope.selectedContact);

    $scope.myEdit = function() {
        $scope.modalShown = true;
    };

    $scope.mySave = function() {
        $scope.selectedContact.$save();
        $scope.modalShown = false;
    };

    $scope.myCancel = function() {
        $scope.modalShown = false;
    };
}
ContactEditController.$inject = ['$scope', '$routeParams', '$resource'];


function MainCtrl($scope) {
    var testVar = 'TestValue';

    $scope.testFn = function() {
        return testVar;
    }
}

'use strict';


// Declare app level module which depends on filters, and services
var myAppModule = angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'ngResource', 'ui']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1',            {templateUrl: 'partials/ContactSearchView.html', controller: ContactSearchController});
    $routeProvider.when('/view2/:contactId', {templateUrl: 'partials/ContactEditView.html', controller: ContactEditController});
    $routeProvider.otherwise({redirectTo: '/view1'});
  }]);

myAppModule.factory('contactSearchService', function() {

    var searchResults = {
        contacts : [],

        pageState : {
            pageNumber: 0,
            pageSize: 15,
            totalPageCount: 0
        }
    };

    var shinyNewServiceInstance = function() {
        return searchResults;
    };
    return shinyNewServiceInstance;
});

/* jasmine specs for controllers go here */

describe('MainCtrl', function () {

    var scope, ctrl;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        ctrl = $controller(MainCtrl, {$scope:scope});
    }));


    it('should return the expected test value when testFn is called', function () {
        expect(scope.testFn()).toEqual('TestValue');
    });
});

describe('ContactSearchController', function () {

    var scope, ctrl;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        ctrl = $controller(ContactSearchController, {$scope:scope});
    }));


    //it('should return the expected test value when testFn is called', function () {
    //    expect(scope.testFn()).toEqual('TestValue');
    //});
});
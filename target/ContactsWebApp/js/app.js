'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'ngResource', 'ui']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1',            {templateUrl: 'partials/ContactSearchView.html', controller: ContactSearchController});
    $routeProvider.when('/view2/:contactId', {templateUrl: 'partials/ContactEditView.html', controller: ContactEditController});
    $routeProvider.otherwise({redirectTo: '/view1'});
  }]);

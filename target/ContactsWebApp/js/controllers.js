'use strict';

/* Controllers */
var globalContacts = [];
var globalPageState = {
  pageNumber: 0,
  pageSize: 15,
  totalPageCount: 0
};

function ContactSearchController($scope, $resource, $http, helloService) {

    $scope.contacts = globalContacts;
    if(!$scope.pageState) {
        console.log('Setting pageState');
        $scope.pageState = globalPageState;
    }
    $scope.pageNumbers = [];

    $scope.previousPage = function() {
        $scope.setPageNumber($scope.pageState.pageNumber - 1);
    }

    $scope.nextPage = function() {
        $scope.setPageNumber($scope.pageState.pageNumber + 1);
    }

    $scope.setPageNumber = function(nextPageNumber) {

        // the service will fix any range issues with pageNumber
        $scope.pageState.pageNumber = nextPageNumber;

        // do the search
        $scope.search();
    }

    $scope.search = function() {

        $http.get('rest/backend/contact?name=' + $scope.name + '&pageNumber=' + $scope.pageState.pageNumber + '&pageSize=' + $scope.pageState.pageSize).success(function(data) {
            var contactsSearchResult = data;
            $scope.contacts = contactsSearchResult['contacts'];
            $scope.pageState = contactsSearchResult.pageState;
            globalContacts = $scope.contacts;

            $scope.pageNumbers = [];
            for(var nextPageNum = 1; nextPageNum <= contactsSearchResult.pageState.totalPageCount; nextPageNum++) {
                $scope.pageNumbers.push({
                    pageNum : nextPageNum,
                    pageClass : ''
                });
            }

            for(var i = 0; i < $scope.pageNumbers.length; i++) {
                var pageMarker = $scope.pageNumbers[i];
                if(i === $scope.pageState.pageNumber) {
                    pageMarker.pageClass = 'active';
                }
                else {
                    pageMarker.pageClass = '';
                }
            }

            console.log('pageNumber = ' + $scope.pageState.pageNumber);
        });
    }

    $scope.search();
}
ContactSearchController.$inject = ['$scope', '$resource', '$http', 'helloService'];


function ContactEditController($scope, $routeParams, $resource, $location) {

    $scope.contactId = $routeParams.contactId;

    this.contactsResource = $resource('rest/backend/contact/:cid', {cid: '@cid'});
    $scope.selectedContact = this.contactsResource.get({cid: $routeParams.contactId});
    console.log($scope.selectedContact);

    $scope.myEdit = function() {
        $scope.modalShown = true;
    }

    $scope.mySave = function() {
        $scope.selectedContact.$save();
        //$location.path('/view1');
        $scope.modalShown = false;
    }

    $scope.myCancel = function() {
        //$location.path('/view1');
        $scope.modalShown = false;
    }
}
ContactEditController.$inject = ['$scope', '$routeParams', '$resource', '$location'];


function MainCtrl($scope) {}
